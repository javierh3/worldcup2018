import moment from 'moment';

function getAPIkeyHeaders() {
	const APIKeyHeader = '42b400167eae43f4901433e510f841df';
	let headers = new Headers();
	headers.append('X-Auth-Token', APIKeyHeader);

	return headers;
}

function getRequestObjectForUrl(url) {
	const myInit = {
		method: 'GET',
		headers: getAPIkeyHeaders(),
		mode: 'cors',
		cache: 'default',
	}

	return new Request(url, myInit);
}

export async function getLeagueMatches() {
	try {
		const response = await fetch(getRequestObjectForUrl('http://api.football-data.org/v1/competitions/467/leagueTable'));
		const responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(error);
	}
}

export async function getTodayScores() {
	try {
		const response = await fetch(getRequestObjectForUrl('http://api.football-data.org/v1/competitions/467/fixtures'));
		const responseJson = await response.json();

		return responseJson.fixtures.filter(
			(f)=> moment().startOf('day').diff(moment(f.date).startOf('day'), 'days') === 0 // JS and dates... :(
		);
	} catch (error) {
		console.error(error);
	}
}