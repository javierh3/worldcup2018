import React from 'react';
import {
	FlatList,
	ActivityIndicator,
	Text,
	View,
	ScrollView,
	StyleSheet,
	RefreshControl,
} from 'react-native';
import moment from 'moment';
import { getTodayScores } from '../api';

export default class TodayMatchesView extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isloading: true,
			refreshing: false,
			scores: [],
		};

		this.setTodayScores = this.setTodayScores.bind(this);
		this.onRefresh = this.onRefresh.bind(this);
	}

	async onRefresh() {
		this.setState({refreshing: true});
		const todayScores = await getTodayScores();
		this.setTodayScores(todayScores);
  }

	setTodayScores(todayScores) {
		this.setState({
			scores: todayScores,
			isloading: false,
			refreshing: false,
		});
	}

	async componentDidMount() {
		const todayScores = await getTodayScores();
		this.setTodayScores(todayScores);
	}

	render() {
		const todayScores = this.state.scores;

		if (this.state.isLoading) {
			return (
				<View style={styles.loading}>
					<ActivityIndicator />
				</View>
			)
		}

		return (
			<ScrollView style={styles.wrapper}
				refreshControl={
					<RefreshControl
						refreshing={this.state.refreshing}
						onRefresh={this.onRefresh}
					/>
				}
			>
				{todayScores.map((s, index) =>
					<View key={index}>
						<Text style={{fontWeight: 'bold'}}>{moment(s.date).format('LLL')}</Text>
						<Text>{s.homeTeamName} VS {s.awayTeamName}</Text>
						<Text>{s.result.goalsHomeTeam || 0} - {s.result.goalsAwayTeam || 0}</Text>
					</View>
				)}
			</ScrollView>
		)
	}
}


const styles = StyleSheet.create({
	loading: { flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' },
	wrapper: { flex: 1, padding: 20 },
	title: { fontSize: 25, fontWeight: 'bold' },
	tableWrapper: { padding: 5 },
	groupTitle: { fontSize: 15, fontWeight: 'bold' }
})