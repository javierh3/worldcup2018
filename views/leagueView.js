import React from 'react';
import {
	FlatList,
	ActivityIndicator,
	Text,
	View,
	ScrollView,
	StyleSheet,
	RefreshControl,
} from 'react-native';

import { getLeagueMatches } from '../api';

export default class LeagueView extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isloading: true,
			refreshing: true,
			leagueTable: {},
		};

		this.setLeagueResults = this.setLeagueResults.bind(this);
		this.onRefresh = this.onRefresh.bind(this);
	}

	async onRefresh() {
		this.setState({refreshing: true});
		const leagueMatches = await getLeagueMatches();
		this.setLeagueResults(leagueMatches);
  }

	setLeagueResults(leagueMatches) {
		this.setState({
			leagueTable: leagueMatches.standings,
			isloading: false,
			refreshing: false,
		});
	}

	async componentDidMount() {
		const leagueMatches = await getLeagueMatches();
		this.setLeagueResults(leagueMatches);
	}

	render() {
		const leagueGroups = this.state.leagueTable;

		if (this.state.isLoading) {
			return (
				<View style={styles.loading}>
					<ActivityIndicator />
				</View>
			)
		}

		return (
			<ScrollView style={styles.wrapper}
				refreshControl={
					<RefreshControl
						refreshing={this.state.refreshing}
						onRefresh={this.onRefresh}
					/>
				}
			>
				{Object.keys(leagueGroups).map((groupName) =>
					<View style={styles.tableWrapper} key={groupName}>
						<Text style={styles.groupTitle}>Group {groupName}</Text>
						<FlatList
							data={this.state.leagueTable[groupName]}
							renderItem={({ item }) => <Text>{item.team} - Points: {item.points}</Text>}
							keyExtractor={(item, index) => item.team}
						/>
					</View>
				)}
			</ScrollView>
		);
	}
}


const styles = StyleSheet.create({
	loading: { flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' },
	wrapper: { flex: 1, paddingHorizontal: 20 },
	title: { fontSize: 25, fontWeight: 'bold' },
	tableWrapper: { padding: 5 },
	groupTitle: { fontSize: 15, fontWeight: 'bold' }
})