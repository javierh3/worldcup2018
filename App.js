import React from 'react';
import { View, StyleSheet, Dimensions, } from 'react-native';
import { TabView } from 'react-native-tab-view';

import { LeagueView, TodayMatchesView } from './views';

export default class App extends React.Component {
  constructor(props) {
    super(props);

		this._renderScene = this._renderScene.bind(this);

    this.state = {
      tabs: {
        index: 0,
        routes: [
          { key: 'league', title: 'League phase' },
          { key: 'today', title: 'Today matches' },
        ],
      }
    };
  }

  _renderScene({ route }) {
    switch (route.key) {
      case 'league':
        return <LeagueView />
      case 'today':
        return <TodayMatchesView />
      default:
        return null
    }
  }

  render() {
    return (
      <TabView
        style={styles.tabsWrapper}
        navigationState={this.state.tabs}
        renderScene={this._renderScene}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{
          width: Dimensions.get('window').width,
          height: Dimensions.get('window').height
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  tabsWrapper: { flex: 1, marginTop: 20 },
})
